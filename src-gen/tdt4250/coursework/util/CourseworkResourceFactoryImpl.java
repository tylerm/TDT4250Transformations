package tdt4250.coursework.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;


public class CourseworkResourceFactoryImpl extends ResourceFactoryImpl {

	
	public CourseworkResourceFactoryImpl() {
		super();
	}
	
	
	@Override
	public Resource createResource(URI uri) {
		Resource result = new CourseworkResourceImpl(uri);
		return result;

	}
}
